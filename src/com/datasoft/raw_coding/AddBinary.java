package com.datasoft.raw_coding;

import java.util.Scanner;

public class AddBinary {
    public static void main(String[] args) {
      long binary1,binary2;
      int i=0, reminder =0;
      int[] sum  = new int[20];

        Scanner input = new Scanner(System.in);
        System.out.print("Input First Binary number: ");
        binary1= input.nextLong();

        System.out.print("Input Second Binary number: ");
        binary2 = input.nextLong();

        while(binary1 != 0 || binary2 != 0){
            sum[i++] = (int) ((binary1 % 10 + binary2 % 10 + reminder) % 2);
            reminder = (int) ((binary1 % 10 + binary2 % 10 + reminder) / 2);
            binary1 = binary1 / 10;
            binary2 = binary2 / 10;
        }
        if(reminder != 0){
            sum[i++] = reminder;
        }
        --i;
        System.out.print("Sum of two binary number:");
        while (i >= 0){
            System.out.print(sum[i--]);

        }
        System.out.println();
    }

}
