package com.datasoft.raw_coding;

import java.nio.charset.Charset;

public class FindAvailableCharSet {
    public static void main(String[] args) {
        System.out.println("List of available charset: ");
        int count =1;
        for (String str: Charset.availableCharsets().keySet()) {
            System.out.println("Count: "+ count + " Charset: "+  str);
            count++;
        }
    }
}
