package com.datasoft.raw_coding;

import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {
        int decimalNumber,rem,qout,i=1;
        int bin_num[] = new int[100];

        Scanner input = new Scanner(System.in);
        System.out.print("Enter the decimal number: ");
        decimalNumber = input.nextInt();

        qout = decimalNumber;

        while (qout != 0){
            bin_num[i++] = qout % 2;
            qout = qout / 2;
        }
        System.out.print("Binary number is: ");
        for (int j = i-1; j > 0 ; j--) {
            System.out.print(bin_num[j]);
        }
        System.out.println();
    }
}
