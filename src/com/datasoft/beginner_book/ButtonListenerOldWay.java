package com.datasoft.beginner_book;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListenerOldWay {
    public static void main(String[] args) {
        Frame frame = new Frame("Action Listener before java 8");

        Button button = new Button("Click Here");

        button.setBounds(50,100,80,50);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Action Performed");
            }
        });

        frame.add(button);

        frame.setSize(200,200);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
