package com.datasoft.beginner_book.methods_reference;

import java.util.function.BiFunction;

public class Example5 {
    public static void main(String[] args) {
        BiFunction<Integer,Integer,Integer> product = Multiplication:: multiply;
        Integer pro = product.apply(15,25);
        System.out.println("Product of Given number is: "+ pro);

    }
}
class Multiplication{
    static Integer multiply(Integer a, Integer b){
       return a*b;
   }

}
