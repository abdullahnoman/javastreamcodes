package com.datasoft.beginner_book.methods_reference;

public class Example7 {
    public static void main(String[] args) {
       // Method reference to a constructor

       MyInterface ref = Hello::new;
       ref.display("Hello Program");
    }
}
interface MyInterface{
   Hello display(String say);
}
class Hello{
    Hello (String say){
        System.out.println(say);
    }
}
