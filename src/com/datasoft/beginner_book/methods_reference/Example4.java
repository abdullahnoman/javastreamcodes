package com.datasoft.beginner_book.methods_reference;

public class Example4 {
    public void myMethod(){
        System.out.println("Instance Method");
    }

    public static void main(String[] args) {
      FunctionReferenceInterface ref = new Example4()::myMethod;
      ref.display();
    }
}
interface FunctionReferenceInterface{
   void display();
}
