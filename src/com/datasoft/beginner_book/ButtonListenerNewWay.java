package com.datasoft.beginner_book;

import java.awt.*;

public class ButtonListenerNewWay {
    public static void main(String[] args) {

        Frame frame = new Frame("Action Listener java 8");

        Button button = new Button("Click Here");

        button.setBounds(50,100,80,50);

        button.addActionListener( e -> System.out.println("Hello World"));


        frame.add(button);

        frame.setSize(200,200);
        frame.setLayout(null);
        frame.setVisible(true);

    }
}
