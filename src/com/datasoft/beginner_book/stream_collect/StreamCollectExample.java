package com.datasoft.beginner_book.stream_collect;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamCollectExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Melisandre","Sansa","Jon","Daenerys","Joffery");

//        List<String> longnames = names.stream()
//                .filter(str -> str.length() > 6)
//                .collect(Collectors.toList());
//
//        longnames.forEach(System.out::println);

        List<String> longnames2 = names.stream()
                .filter(str -> str.length() > 5 && str.length()< 8)
                .collect(Collectors.toList());

        longnames2.forEach(System.out::println);

        List<Integer> num = Arrays.asList(1,2,3,4,5,6);
        List<Integer> squares = num.stream()
                .map(n -> n*n)
                .collect(Collectors.toList());

        squares.forEach(System.out::println);



    }
}
