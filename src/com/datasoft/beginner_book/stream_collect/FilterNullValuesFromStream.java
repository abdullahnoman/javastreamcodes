package com.datasoft.beginner_book.stream_collect;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FilterNullValuesFromStream {
    public static void main(String[] args) {

        /*
        * Null value printing
        * */
        List<String> list = Arrays.asList("Java","Stream",null,"Filter",null);

        List<String> result = list.stream().collect(Collectors.toList());

        result.forEach(s -> System.out.println(s));


        System.out.println("\n\n\nCheck Null value");

        /*
        * Filter null values with Stream Api
        * */

        List<String> checkNullValueResult = list
                .stream()
                .filter( str -> str !=null)
                .collect(Collectors.toList());
        checkNullValueResult.forEach(s -> System.out.println(" "+ s));


        // Using Method reference
        List<String> checkNullValueWithMethodReference = list.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        checkNullValueWithMethodReference.forEach(s -> System.out.println(""+s));

        /**
         * Filter null values with map intermediate operation
         * **/

        List<Integer> list1 = Arrays.asList(1,2,3,null,4,null,5);
        List<Integer> resultMapIntermediateOperation = list1.stream()
                .map(num -> num) // here you will have different logic
                .filter( n -> n != null)
                .collect(Collectors.toList());

        resultMapIntermediateOperation.forEach(s-> System.out.println(""+s));


    }
}
