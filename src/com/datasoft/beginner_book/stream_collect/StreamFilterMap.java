package com.datasoft.beginner_book.stream_collect;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamFilterMap {
    public static void main(String[] args) {
//        Map<Integer,String> hmap = new HashMap<Integer, String>();
//
//        hmap.put(11, "Apple");
//        hmap.put(22,"Orange");
//        hmap.put(33, "Kiwi");
//        hmap.put(44, "Banana");
//
//
//        Map<Integer,String> result = hmap.entrySet()
//                .stream()
//                .filter(map -> map.getKey().intValue() <= 22)
//                .collect(Collectors.toMap(map -> map.getKey(),map -> map.getValue()));
//
//        System.out.println("Result : " + result);

       Map<Integer,String> hmap = new HashMap<Integer, String>();
       hmap.put(11,"Apple");
       hmap.put(22,"Orange");
       hmap.put(33,"Kiwi");
       hmap.put(44,"Banana");


       Map<Integer,String> result = hmap.entrySet()
               .stream()
               .filter(map -> "Orange".equals(map.getValue()))
               .collect(Collectors.toMap(map -> map.getKey(),map -> map.getValue()));

        System.out.println("Result: " + result);


        Map<Integer,String> hmap2 = new HashMap<>();
        hmap2.put(1,"ABC");
        hmap2.put(2,"XCB");
        hmap2.put(3,"ABB");
        hmap2.put(4,"ZIO");

        Map<Integer,String> result2 = hmap2.entrySet()
                .stream()
                .filter(p -> p.getKey() <= 2 )
                .filter(map -> map.getValue().startsWith("A"))
                .collect(Collectors.toMap(map -> map.getKey(),map -> map.getValue()));

        System.out.println("Result2: " + result2);


    }
}
