package com.datasoft.beginner_book.exmple_one;

public class Example3 {
    public static void main(String[] args) {

        MyFunctionalInterface3 myFunctionalInterface3 = (a,b) -> a + b;

        System.out.println(myFunctionalInterface3.strConcat("Hello " ," Message"));

    }
}
interface MyFunctionalInterface3{
    public String strConcat(String a, String b);
}
