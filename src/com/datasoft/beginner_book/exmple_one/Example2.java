package com.datasoft.beginner_book.exmple_one;

public class Example2 {
    public static void main(String[] args) {
        MyFuntionalInterface2 myFuntionalInterface2  = (num) -> num+5;

        System.out.println(myFuntionalInterface2.incrementBy5(25));
    }
}
interface MyFuntionalInterface2{
    int incrementBy5(int a);
}
