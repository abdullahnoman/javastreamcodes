package com.datasoft.beginner_book.exmple_one;

import java.util.ArrayList;
import java.util.List;

public class ListIteration {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();

        list.add("First");
        list.add("Second");
        list.add("Third");
        list.add("Fourth");
//
//        list.forEach(
//                (names) -> System.out.println(names)
//        );

//        list.forEach((names) -> System.out.println(names));
        list.forEach((names) -> System.out.println(names));

        List<String> list2 = new ArrayList<>();
        list2.add("Data 1");
        list2.add("Data 2");
        list2.add("Data 3");

        list2.forEach((data) -> System.out.println(data));
    }
}
