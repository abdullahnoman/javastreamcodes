package com.datasoft.beginner_book.exmple_one;

public class Example {
    public static void main(String[] args) {
          MyFunctionalInterface msg = () ->{
              return "Hello World";
          };

        System.out.println(msg.sayHello());


    }
}
interface MyFunctionalInterface{
    public String sayHello();

    static void printText(){
        System.out.println("Hello");
    }
}
