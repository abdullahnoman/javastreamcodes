package com.datasoft.beginner_book.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample3 {
    public static void main(String[] args) {
        List<String> alphabets = Arrays.asList("A","B","C");

        List<String> names = Arrays.asList("Sansa","John","Arya");

        Stream<String> opstream = Stream.concat(alphabets.stream(),names.stream());

        // Displaying the elements of the concatenated string
        opstream.forEach(str-> System.out.print(str+ " "));
    }
}
