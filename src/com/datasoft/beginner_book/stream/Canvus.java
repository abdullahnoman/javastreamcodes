package com.datasoft.beginner_book.stream;


import java.util.stream.Stream;

public class Canvus {
    public static void main(String[] args) {

        Stream.iterate(1, count -> count+1)
                .filter(number -> number%2 ==0)
                .limit(50)
                .forEach(System.out::println);

    }
}
