package com.datasoft.beginner_book.builder_pattern;

public class Wrapper implements Packing{

    @Override
    public String pack() {
        return "Wrapper";
    }

}
