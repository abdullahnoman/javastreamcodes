package com.datasoft.beginner_book.builder_pattern;

public interface Packing {
    public String pack();
}
