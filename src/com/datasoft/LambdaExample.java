package com.datasoft;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class LambdaExample {
    public static void main(String[] args) {

        MyInterface myInterface = (String text) ->{
              System.out.println(text);
          };

        myInterface.printIt("Hello Text");

        try {
            myInterface.printUtf8To("Data has been written into the text file", new FileOutputStream("data.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        myInterface.printIt("Text Data");

    }
}
